<?php

require 'vendor/autoload.php';

use UploaderBuilder\Builder;

$target = __DIR__ . DIRECTORY_SEPARATOR . 'build';
$src    = __DIR__ . DIRECTORY_SEPARATOR . 'uploader';

Builder::create('uploader.phar', $target, $src)
    ->handle()
    ->createArchive('index.php')
    ->cleanUp()
;
