<?php

require 'vendor/autoload.php';

use Symfony\Component\Console\Application;
use Uploader\Command as Commands;
use Uploader\Files\ConfigManager;

function getBaseDirectory()
{
    $pharFilename = Phar::running(false);

    if ($pharFilename === "") {
        return __DIR__;
    }

    return pathinfo($pharFilename, PATHINFO_DIRNAME);
}

$console = new Application();
$baseDirectory = getBaseDirectory();
$config = ConfigManager::getParameters($baseDirectory);

$console->addCommands([
    new Commands\DumpParametersCommand('dump-parameters', $baseDirectory, $config),
    new Commands\ListParametersCommand('list-parameters', $baseDirectory, $config),
    new Commands\LoadCommand('load', $baseDirectory, $config),
    new Commands\RestoreCommand('restore', $baseDirectory, $config),
]);

$console->run();
