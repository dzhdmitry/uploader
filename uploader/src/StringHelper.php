<?php

namespace Uploader;

class StringHelper
{
    /**
     * @param $filename
     * @return string|null
     */
    public static function getNameWithoutExtension($filename)
    {
        $dotPosition = mb_strrpos($filename, ".");

        if ($dotPosition === false) {
            return null;
        }

        return mb_substr($filename, 0, $dotPosition);
    }

    /**
     * @return string
     */
    public static function joinPath()
    {
        $args = func_get_args();

        return implode(DIRECTORY_SEPARATOR, $args);
    }
}
