<?php

namespace Uploader\Database;

use Uploader\UploaderException;

class Connection
{
    /**
     * @var \PDO
     */
    private $db;

    public function __construct($config, $database = null)
    {
        $dsn = "pgsql:host=" . $config["host"];

        if ($database) {
            $dsn .= " dbname=" . $database;
        }

        $this->db = new \PDO($dsn, $config["user"], $config["password"]);
    }

    /**
     * @param string $query
     * @return \PDOStatement
     */
    public function prepare($query)
    {
        return $this->db->prepare($query);
    }

    /**
     * @param \PDOStatement $statement
     * @param array $parameters
     * @return bool
     * @throws UploaderException
     */
    public static function execute(\PDOStatement $statement, $parameters = null)
    {
        $result = $statement->execute($parameters);

        if (!$result) {
            $message = implode("\n", $statement->errorInfo());

            throw UploaderException::create($message);
        }

        return $result;
    }

    public function stop()
    {
        $this->db = null;
    }
}
