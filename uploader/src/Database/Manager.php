<?php

namespace Uploader\Database;

class Manager
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $name
     */
    public function createDatabase($name)
    {
        $query = sprintf('CREATE DATABASE "%s"', $name);
        $statement = $this->connection->prepare($query);

        Connection::execute($statement);
    }

    /**
     * @param string $name
     * @param string $newName
     */
    public function renameDatabase($name, $newName)
    {
        $query = sprintf('ALTER DATABASE "%s" RENAME TO "%s"', $name, $newName);
        $statement = $this->connection->prepare($query);

        Connection::execute($statement);
    }

    /**
     * @param string $name
     */
    public function dropDatabase($name)
    {
        $query = sprintf('DROP DATABASE "%s"', $name);
        $statement = $this->connection->prepare($query);

        Connection::execute($statement);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function databaseExists($name)
    {
        $query = 'SELECT datname FROM pg_catalog.pg_database WHERE lower(datname) = lower(:name)';
        $statement = $this->connection->prepare($query);

        $statement->bindParam('name', $name);
        Connection::execute($statement);

        $results = $statement->fetchAll();

        return count($results) > 0;
    }

    /**
     * @return array[]
     */
    public function getExtensions()
    {
        $query = 'SELECT * FROM pg_available_extensions';
        $statement = $this->connection->prepare($query);

        Connection::execute($statement);

        $results = $statement->fetchAll();

        return $results;
    }

    /**
     * @param string $extension
     */
    public function loadExtension($extension)
    {
        $query = sprintf('CREATE EXTENSION %s', $extension);
        $statement = $this->connection->prepare($query);

        Connection::execute($statement);
    }

    /**
     * @param string[] $extensions
     */
    public function loadExtensions($extensions)
    {
        foreach ($extensions as $extension) {
            $this->loadExtension($extension);
        }
    }
}
