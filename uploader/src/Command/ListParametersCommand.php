<?php

namespace Uploader\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListParametersCommand extends BaseCommand
{
    private static $captions = [
        'connection' => "Database connection",
        'extensions' => "Database extensions"
    ];

    protected function configure()
    {
        $this
            ->setDescription('<info>Show parameters</info>.')
            ->setHelp(<<<EOT
Show parameters used for dump loading
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setOutput($output);
        $this->writeln("<comment>Parameters:</comment>");

        foreach ($this->config as $name => $parameter) {
            if (is_array($parameter)) {
                $this->writeln("  <comment>%s:</comment>", self::getCaption($name));

                foreach ($parameter as $subName => $subParameter) {
                    $this->writeLine($subName, $subParameter, 4);
                }
            } else {
                $this->writeLine($name, $parameter);
            }
        }
    }

    /**
     * @param $name
     * @param $parameter
     * @param int $offset
     */
    private function writeLine($name, $parameter, $offset = 2)
    {
        $caption = self::getCaption($name);
        $parameterLiteral = empty($parameter) ? "~" : $parameter;
        $format = 40 - $offset;
        $offsetStr = "";

        for ($i=0; $i<$offset; $i++) {
            $offsetStr .= " ";
        }

        $this->writeln("%s<info>%-" . $format . "s</info>%s", $offsetStr, $caption, $parameterLiteral);
    }

    /**
     * @param $name
     * @return string
     */
    private static function getCaption($name)
    {
        if (is_int($name)) {
            return "-";
        }

        if (array_key_exists($name, self::$captions)) {
            $caption = self::$captions[$name];
        } else {
            $caption = $name;
        }

        return $caption;
    }
}
