<?php

namespace Uploader\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Uploader\Files\ConfigManager;

class DumpParametersCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setDescription('<info>Create file with default parameters</info>.')
            ->setHelp(<<<EOT
Creates parameters.yml with default parameters in root directory
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setOutput($output);

        $returnCode = ConfigManager::dumpParameters($this->baseDirectory);

        if ($returnCode === 0) {
            $this->writeln("<info>Parameters dumped into parameters.yml</info>");
        }
    }
}
