<?php

namespace Uploader\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Uploader\Database\Connection;
use Uploader\Database\Manager;
use Uploader\Files\DumpManager;
use Uploader\UploaderException;

class LoadCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setDescription('<info>Load dump file to the database</info>.')
            ->addArgument('database', InputArgument::REQUIRED, 'Database name.')
            ->addOption('file_pattern', 'p', InputOption::VALUE_OPTIONAL, 'Find files by name compatible with this pattern.', $this->config["file_pattern"])
            ->addOption('target', 't', InputOption::VALUE_OPTIONAL, 'Find files inside this directory (root directory if not provided).', $this->config["target"])
            ->addOption('psql', 's', InputOption::VALUE_OPTIONAL, 'Path to psql (run from root directory if not provided).', $this->config["psql"])
            ->addOption('backup_postfix', 'b', InputOption::VALUE_OPTIONAL, 'Postfix appended to backup database name.', $this->config["backup_postfix"])
            ->setHelp(<<<EOT
Find files by name compatible with `file_pattern` in `target` directory;
Extract last file;
Create backup of existing database;
Create database (`database` argument), create extensions;
Load extracted dump file into the database.
EOT
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setInput($input);
        $this->setOutput($output);

        $database = $input->getArgument('database');
        $parameters = $this->handleParameters(['file_pattern', 'target', 'psql', 'backup_postfix']);

        try {
            $this->validateParameters($parameters);

            $connection = new Connection($parameters["connection"]);
            $databasesManager = new Manager($connection);
            $dumpManager = new DumpManager($parameters, $this->baseDirectory);
            $backupDatabase = $database . $parameters["backup_postfix"];
        } catch (UploaderException $e) {
            $this->writeln("<error>%s</error>", $e->getMessage());

            return 1;
        }

        $doExtractArchive = true;
        $dbBackupCreated = false;
        $dbConnection = null;

        try {
            if ($dumpManager->extractedDumpExists()) {
                $extracted = $dumpManager->getDumpFilename();

                if ($this->confirm(sprintf('Extracted file `%s` found. Load it instead (y/n)?', $extracted))) {
                    $doExtractArchive = false;
                }
            }

            if ($doExtractArchive) {
                $inFilename = $dumpManager->getArchiveFilename();

                if ($dumpManager->mustBeExtracted()) {
                    $this->writeln("Extracting `%s` dump file...", $inFilename);
                    $dumpManager->extract();
                } else {
                    $dumpManager->setDumpFilename($inFilename);

                    $doExtractArchive = false;
                }
            }

            if ($databasesManager->databaseExists($backupDatabase)) {
                $databasesManager->dropDatabase($backupDatabase);
            }

            if ($databasesManager->databaseExists($database)) {
                $this->writeln("Creating backup of database `%s` to `%s`...", $database, $backupDatabase);
                $databasesManager->renameDatabase($database, $backupDatabase);

                $dbBackupCreated = true;
            }

            $this->writeln("Creating database `%s` from scratch...", $database);
            $databasesManager->createDatabase($database);

            $dbConnection = new Connection($parameters["connection"], $database);

            $this->loadExtensions($dbConnection, $parameters["extensions"]);

            $this->writeln("Loading dump...");
            $dumpManager->loadDump($database);

            if ($doExtractArchive) {
                $dumpManager->removeDump();
            }
        } catch (UploaderException $e) {
            $this->writeln("<error>%s</error>", $e->getMessage());

            if ($dbBackupCreated) {
                $this->writeln("Rolling back backup database...", $backupDatabase, $database);

                if ($databasesManager->databaseExists($database)) {
                    if ($dbConnection) {
                        $dbConnection->stop();
                    }

                    $databasesManager->dropDatabase($database);
                }

                $databasesManager->renameDatabase($backupDatabase, $database);
            }

            if ($doExtractArchive) {
                $dumpManager->removeDump();
            }

            return 1;
        }

        $this->writeln("<info>Dump file successfully loaded</info>");

        return 0;
    }

    /**
     * @param array $parameters
     * @throws UploaderException
     */
    private function validateParameters($parameters)
    {
        if (!array_key_exists('file_pattern', $parameters) || empty($parameters['file_pattern'])) {
            throw UploaderException::create('Parameter `file_pattern` must not be empty');
        }

        if (!array_key_exists('backup_postfix', $parameters) || empty($parameters['backup_postfix'])) {
            throw UploaderException::create('Parameter `backup_postfix` must not be empty');
        }
    }

    /**
     * @param Connection $dbConnection
     * @param string[] $extensions
     */
    private function loadExtensions(Connection $dbConnection, $extensions)
    {
        $manager = new Manager($dbConnection);
        $loadableExtensions = [];
        $available = array_column($manager->getExtensions(), 'name');

        foreach ($extensions as $extension) {
            if (in_array($extension, $available)) {
                $loadableExtensions[] = $extension;
            } else {
                $this->writeln("<comment>Warning: extension `%s` does not exist in database and will not be loaded</comment>", $extension);
            }
        }

        if ($loadableExtensions) {
            return;
        }

        $this->writeln("Loading extensions...");
        $manager->loadExtensions($extensions);
    }
}
