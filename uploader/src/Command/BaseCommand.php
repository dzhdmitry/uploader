<?php

namespace Uploader\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class BaseCommand extends Command
{
    /**
     * @var string
     */
    protected $baseDirectory;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var InputInterface|null
     */
    protected $input;

    /**
     * @var OutputInterface|null
     */
    protected $output;

    public function __construct($name = null, $baseDirectory = '', $config = [])
    {
        parent::__construct($name);

        $this->baseDirectory = $baseDirectory;
        $this->config = $config;
    }

    /**
     * @param string $message
     * @return bool
     */
    public function confirm($message)
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion($message);

        return !!$helper->ask($this->input, $this->output, $question);
    }

    /**
     * @param InputInterface $input
     */
    public function setInput(InputInterface $input)
    {
        $this->input = $input;
    }

    /**
     * @param OutputInterface $output
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function writeln()
    {
        $args = func_get_args();

        if (count($args) > 0) {
            $format = array_shift($args);
            $message = vsprintf($format, $args);
        } else {
            $message = "";
        }

        $this->output->writeln($message);
    }

    /**
     * @param $optionsNames
     * @return array
     */
    protected function handleParameters($optionsNames)
    {
        $parameters = $this->config;

        foreach ($optionsNames as $name) {
            if (!$this->input->hasOption($name)) {
                continue;
            }

            if ($option = $this->input->getOption($name)) {
                $parameters[$name] = $this->input->getOption($name);
            }
        }

        return $parameters;
    }
}
