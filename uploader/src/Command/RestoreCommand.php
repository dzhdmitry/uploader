<?php

namespace Uploader\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Uploader\Database\Connection;
use Uploader\Database\Manager;
use Uploader\UploaderException;

class RestoreCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setDescription('<info>Restore the database from backup</info>.')
            ->addArgument('database', InputArgument::REQUIRED, 'Database name.')
            ->addOption('backup_postfix', 'b', InputOption::VALUE_OPTIONAL, 'Postfix appended to backup database name.', $this->config["backup_postfix"])
            ->setHelp(<<<EOT
Rename backup database to its original name
EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setInput($input);
        $this->setOutput($output);

        $database = $input->getArgument('database');
        $parameters = $this->handleParameters(['backup_postfix']);

        $connection = new Connection($parameters["connection"]);
        $databaseManager = new Manager($connection);
        $backupDatabase = $database . $parameters["backup_postfix"];

        try {
            if (!$databaseManager->databaseExists($backupDatabase)) {
                $this->writeln("<error>Backup for `%s` database not found</error>", $database);

                return 1;
            }

            $this->writeln("Rolling back backup database...", $backupDatabase, $database);

            if ($databaseManager->databaseExists($database)) {
                if ($this->confirm("Original database exists. Replace it by backup?")) {
                    $databaseManager->dropDatabase($database);
                }
            }

            $databaseManager->renameDatabase($backupDatabase, $database);
        } catch (UploaderException $e) {
            return 1;
        }

        $this->writeln("<info>Backup database successfully restored</info>");

        return 0;
    }
}
