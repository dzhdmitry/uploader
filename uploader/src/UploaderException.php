<?php

namespace Uploader;

class UploaderException extends \Exception
{
    /**
     * @return UploaderException
     */
    public static function create()
    {
        $args = func_get_args();

        if (count($args) > 0) {
            $format = array_shift($args);
            $message = vsprintf($format, $args);
        } else {
            $message = "";
        }

        return new self($message);
    }
}
