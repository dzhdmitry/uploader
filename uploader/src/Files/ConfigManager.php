<?php

namespace Uploader\Files;

use Symfony\Component\Yaml\Yaml;
use Uploader\StringHelper;
use Uploader\UploaderException;

class ConfigManager
{
    const PARAMETERS_FILENAME = "parameters.yml";

    const PATH_TO_PARAMETERS = __DIR__ . "/../../config/parameters.yml";

    /**
     * @param string $path
     * @return array
     */
    public static function getParameters($path)
    {
        $defaultParameters = self::getDefaultParameters();
        $filePath = StringHelper::joinPath($path, self::PARAMETERS_FILENAME);

        if (!file_exists($filePath)) {
            return $defaultParameters;
        }

        $input = file_get_contents($filePath);
        $parameters = Yaml::parse($input);

        return array_merge($defaultParameters, $parameters);
    }

    /**
     * @param string $path
     * @return int
     * @throws UploaderException
     */
    public static function dumpParameters($path)
    {
        $data = self::getDefaultParametersContent();
        $filePath = StringHelper::joinPath($path, self::PARAMETERS_FILENAME);
        $return = file_put_contents($filePath, $data);

        return ($return !== false) ? 0 : 1;
    }

    /**
     * @return string
     * @throws UploaderException
     */
    private static function getDefaultParametersContent()
    {
        if (!file_exists(self::PATH_TO_PARAMETERS)) {
            throw UploaderException::create("File `%s` not found", self::PATH_TO_PARAMETERS);
        }

        return file_get_contents(self::PATH_TO_PARAMETERS);
    }

    /**
     * @return array
     */
    private static function getDefaultParameters()
    {
        $input = self::getDefaultParametersContent();

        return Yaml::parse($input);
    }
}
