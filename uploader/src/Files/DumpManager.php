<?php

namespace Uploader\Files;

use Uploader\Files\Adapter\BaseAdapter;
use Uploader\StringHelper;
use Uploader\UploaderException;

class DumpManager
{
    const DEFAULT_DUMP_FILENAME = 'backup.sql';

    /**
     * @var string
     */
    private $baseDirectory;

    /**
     * @var array
     */
    private $connection;

    /**
     * @var string
     */
    private $pathToPSQL;

    /**
     * @var string
     */
    private $pathToDumpFile;

    /**
     * @var string
     */
    private $filePattern;

    /**
     * @var string|null
     */
    private $archiveFilename;

    /**
     * @var string|null
     */
    private $dumpFilename;

    public function __construct($config, $baseDirectory)
    {
        $pathToDumpFile = $baseDirectory;

        if ($config["target"]) {
            $pathToDumpFile .= $config["target"];
        }

        $this->baseDirectory = $baseDirectory;
        $this->pathToDumpFile = $pathToDumpFile;
        $this->connection = $config["connection"];
        $this->pathToPSQL = $config["psql"];
        $this->filePattern = $config["file_pattern"];
    }

    /**
     * @return bool
     */
    public function extractedDumpExists()
    {
        $outFilename = $this->getPathToDump();

        return file_exists($outFilename);
    }

    /**
     * @return bool
     */
    public function mustBeExtracted()
    {
        $inFilename = $this->getPathToArchive();
        $type = mime_content_type($inFilename);

        return ($type !== 'text/plain');
    }

    public function extract()
    {
        $inFilename = $this->getPathToArchive();

        if (!is_readable($inFilename)) {
            throw UploaderException::create('File `%s` is not readable', $inFilename);
        }

        if (!is_writable($this->pathToDumpFile)) {
            throw UploaderException::create('Directory `%s` is not writable', $this->pathToDumpFile);
        }

        $adapter = BaseAdapter::createAdapter($inFilename)
            ->setDestination($this->pathToDumpFile)
            ->setOutFilename($this->getDumpFilename());

        $this->dumpFilename = $adapter->extract();
    }

    public function removeDump()
    {
        $filename = $this->getPathToDump();

        if (file_exists($filename)) {
            @unlink($filename);
        }
    }

    /**
     * @param string $database
     * @throws UploaderException
     */
    public function loadDump($database)
    {
        $user = $this->connection["user"];
        $pathToDump = $this->getPathToDump();
        $command = sprintf("psql %s < %s %s", $database, $pathToDump, $user);
        $output = null;

        if ($this->pathToPSQL) {
            chdir($this->pathToPSQL);
        }

        exec($command, $output, $returnCode);

        if ($this->pathToPSQL) {
            chdir($this->baseDirectory);
        }

        if ($returnCode != 0) {
            throw UploaderException::create("Command `%s` returned code %d", $command, $returnCode);
        }
    }

    /**
     * @return string|null
     */
    public function getArchiveFilename()
    {
        $this->initializeArchiveFilename();

        return $this->archiveFilename;
    }

    /**
     * @return string|null
     */
    public function getDumpFilename()
    {
        $this->initializeDumpFilename();

        return $this->dumpFilename;
    }

    /**
     * @return string
     */
    public function getPathToArchive()
    {
        return StringHelper::joinPath($this->pathToDumpFile, $this->getArchiveFilename());
    }

    /**
     * @return string
     */
    public function getPathToDump()
    {
        return StringHelper::joinPath($this->pathToDumpFile, $this->getDumpFilename());
    }

    /**
     * @param string $filename
     */
    public function setDumpFilename($filename)
    {
        $this->dumpFilename = $filename;
    }

    private function initializeArchiveFilename()
    {
        if (!$this->archiveFilename) {
            if (!($this->archiveFilename = $this->retrieveArchiveFilename($this->pathToDumpFile))) {
                throw UploaderException::create("Archive at `%s` not found (search by `%s` pattern)", $this->pathToDumpFile, $this->filePattern);
            }
        }
    }

    private function initializeDumpFilename()
    {
        if (!$this->dumpFilename) {
            $this->initializeArchiveFilename();

            $dumpFilename = StringHelper::getNameWithoutExtension($this->archiveFilename);
            $this->dumpFilename = $dumpFilename ? $dumpFilename : self::DEFAULT_DUMP_FILENAME;
        }
    }

    /**
     * @param string $path
     * @return string|null
     */
    private function retrieveArchiveFilename($path)
    {
        $filename = null;

        if ($handle = opendir($path)) {
            $files = [];

            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    if (@preg_match($this->filePattern, $entry)) {
                        $files[] = $entry;
                    }
                }
            }

            closedir($handle);

            if (count($files) > 0) {
                $filename = $files[count($files) - 1];
            }
        }

        return $filename;
    }
}
