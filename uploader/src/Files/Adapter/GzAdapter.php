<?php

namespace Uploader\Files\Adapter;

use Uploader\StringHelper;
use Uploader\UploaderException;

class GzAdapter extends BaseAdapter
{
    const GZ_BUFFER_SIZE = 4096;

    /**
     * @return string
     * @throws UploaderException
     */
    public function extract()
    {
        $outFilename = StringHelper::joinPath($this->destination, $this->outFilename);
        $inFile = gzopen($this->inFilename, 'rb');
        $outFile = fopen($outFilename, 'wb');

        if ($inFile === false) {
            throw UploaderException::create('Could not open file `%s` for extracting', $this->inFilename);
        }

        if ($outFile === false) {
            throw UploaderException::create('Could not open file `%s` for writing', $outFilename);
        }

        while (!gzeof($inFile)) {
            fwrite($outFile, gzread($inFile, self::GZ_BUFFER_SIZE));
        }

        fclose($outFile);
        gzclose($inFile);

        return $this->outFilename;
    }
}
