<?php

namespace Uploader\Files\Adapter;

use Uploader\UploaderException;

abstract class BaseAdapter
{
    const MIME_ZIP = 'application/zip';

    const MIME_GZ = 'application/x-gzip';

    /**
     * @var string
     */
    protected $inFilename;

    /**
     * @var string
     */
    protected $destination;

    /**
     * @var string
     */
    protected $outFilename;

    public function __construct($filename)
    {
        $this->inFilename = $filename;
    }

    /**
     * @param string $filename
     * @return BaseAdapter
     * @throws UploaderException
     */
    public static function createAdapter($filename)
    {
        $type = mime_content_type($filename);

        switch ($type) {
            case BaseAdapter::MIME_ZIP:
                $adapter = new ZipAdapter($filename);

                break;
            case BaseAdapter::MIME_GZ:
                $adapter = new GzAdapter($filename);

                break;
            default:
                throw UploaderException::create('Type `%s` of archive is not supported', $type);
        }

        return $adapter;
    }

    /**
     * @param $destination
     * @return BaseAdapter
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * @param $outFilename
     * @return BaseAdapter
     */
    public function setOutFilename($outFilename)
    {
        $this->outFilename = $outFilename;

        return $this;
    }

    /**
     * @return string
     */
    abstract public function extract();
}
