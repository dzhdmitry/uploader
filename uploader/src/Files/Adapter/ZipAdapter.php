<?php

namespace Uploader\Files\Adapter;

use Uploader\UploaderException;
use ZipArchive;

class ZipAdapter extends BaseAdapter
{
    /**
     * @return string
     * @throws UploaderException
     */
    public function extract()
    {
        $zip = new ZipArchive();

        if ($zip->open($this->inFilename) !== true) {
            throw UploaderException::create('Cannot extract zip-archive');
        }

        if ($zip->numFiles == 0) {
            throw UploaderException::create('Archive `%s` is empty', $this->inFilename);
        }

        if ($zip->numFiles > 1) {
            throw UploaderException::create('Archive `%s` contains more than 1 entry', $this->inFilename);
        }

        $filename = $zip->getNameIndex(0);

        $zip->extractTo($this->destination);
        $zip->close();

        return $filename;
    }
}
