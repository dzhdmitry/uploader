<?php

namespace UploaderBuilder\SourceHandler;

use UploaderBuilder\UploaderDirectoryIterator;

abstract class BaseHandler
{
    /**
     * @var string
     */
    protected $destination;

    /**
     * @var string
     */
    protected $source;

    public function __construct($destination, $srcDir)
    {
        $this->destination = $destination;
        $this->source = $srcDir;
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    public function createDirectory()
    {
        $this->removeDirectory();

        mkdir($this->destination);
    }

    public function removeDirectory()
    {
        if (!file_exists($this->destination)) {
            return;
        }

        UploaderDirectoryIterator::create($this->destination, \RecursiveDirectoryIterator::SKIP_DOTS, \RecursiveIteratorIterator::CHILD_FIRST)
            ->iterate(function(\SplFileInfo $item) {
                if ($item->isDir()) {
                    rmdir($item->getRealPath());
                } else {
                    unlink($item->getRealPath());
                }
            });

        rmdir($this->destination);
    }

    abstract public function handle();
}
