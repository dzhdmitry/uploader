<?php

namespace UploaderBuilder\SourceHandler;

use UploaderBuilder\UploaderDirectoryIterator;

class SourceHandler extends BaseHandler
{
    private $excludedDirectories = [
        'Tests'
    ];

    private $excludedFiles = [
        '.gitignore',
        'CHANGELOG.md',
        'composer.json',
        'composer.lock',
        'LICENSE',
        'README.md',
    ];

    public function handle()
    {
        UploaderDirectoryIterator::create($this->source)->iterate(function(\SplFileInfo $item) {
            $destination = $this->destination . str_replace($this->source, '', $item->getPathname());

            if ($this->isExcluded($item)) {
                return;
            }

            if ($item->isDir()) {
                if (!file_exists($destination)) {
                    mkdir($destination);
                }
            } else {
                copy($item->getPathname(), $destination);
            }
        });
    }

    /**
     * @param \SplFileInfo $info
     * @return bool
     */
    protected function isExcluded(\SplFileInfo $info) {
        if ($info->isFile()) {
            if (in_array($info->getBasename(), $this->excludedFiles)) {
                return true;
            }
        }

        $pathName = $info->getPathname();
        $excluded = false;

        foreach ($this->excludedDirectories as $excludedDirectory) {
            $pos = strpos($pathName, DIRECTORY_SEPARATOR . $excludedDirectory . DIRECTORY_SEPARATOR);

            if ($pos !== false) {
                $excluded = true;

                break;
            }
        }

        return $excluded;
    }
}
