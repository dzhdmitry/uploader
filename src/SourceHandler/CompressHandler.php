<?php

namespace UploaderBuilder\SourceHandler;

class CompressHandler extends BaseHandler
{
    public function handle()
    {
        $phpMinify = new \PhpMinify([
            'source' => $this->source,
            'target' => $this->destination
        ]);

        $phpMinify->run();
    }
}
