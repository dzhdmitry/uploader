<?php

namespace UploaderBuilder;

use UploaderBuilder\SourceHandler\BaseHandler;
use UploaderBuilder\SourceHandler\CompressHandler;
use UploaderBuilder\SourceHandler\SourceHandler;

class Builder
{
    /**
     * @var string
     */
    private $target;

    /**
     * @var string
     */
    private $targetArchive;

    /**
     * @var BaseHandler[]
     */
    private $handlers;

    public function __construct($target, $targetDir, $srcDir)
    {
        $tempDir = $targetDir . DIRECTORY_SEPARATOR . '__tmp';
        $minDir = $targetDir . DIRECTORY_SEPARATOR . '__min';

        $this->target = $target;
        $this->targetArchive = $targetDir . DIRECTORY_SEPARATOR . $target;

        $this->handlers[] = new SourceHandler($tempDir, $srcDir);
        $this->handlers[] = new CompressHandler($minDir, $tempDir);
    }

    /**
     * @param string $target
     * @param string $targetDir
     * @param string $srcDir
     * @return Builder
     */
    public static function create($target, $targetDir, $srcDir)
    {
        return new self($target, $targetDir, $srcDir);
    }

    /**
     * @return Builder
     */
    public function handle()
    {
        foreach ($this->handlers as $handler) {
            $handler->createDirectory();
            $handler->handle();
        }

        return $this;
    }

    /**
     * @return Builder
     */
    public function cleanUp()
    {
        foreach ($this->handlers as $handler) {
            $handler->removeDirectory();
        }

        return $this;
    }

    /**
     * @return BaseHandler
     */
    public function getLastHandler()
    {
        return $this->handlers[count($this->handlers) - 1];
    }

    /**
     * @param string $indexFile
     * @return Builder
     */
    public function createArchive($indexFile)
    {
        $this->removeArchive();

        $phar = new \Phar($this->targetArchive, 0, $this->target);

        $phar->buildFromDirectory($this->getLastHandler()->getDestination());
        $phar->setStub($phar->createDefaultStub($indexFile));

        return $this;
    }

    /**
     * @return Builder
     */
    public function removeArchive()
    {
        if (file_exists($this->targetArchive)) {
            unlink($this->targetArchive);
        }

        return $this;
    }
}
