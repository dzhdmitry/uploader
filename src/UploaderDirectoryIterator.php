<?php

namespace UploaderBuilder;

class UploaderDirectoryIterator
{
    /**
     * @var \RecursiveIteratorIterator
     */
    private $iterator;

    public function __construct($path, $directoryFlags = null, $flags = null)
    {
        $this->iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($path, $directoryFlags), $flags);
    }

    /**
     * @param string $path
     * @param int $directoryFlags
     * @param int $flags
     * @return UploaderDirectoryIterator
     */
    public static function create($path, $directoryFlags = null, $flags = null)
    {
        return new self($path, $directoryFlags, $flags);
    }

    /**
     * @param $function
     */
    public function iterate($function)
    {
        foreach ($this->iterator as $item) {
            $function($item);
        }
    }
}
